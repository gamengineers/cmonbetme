import Vue from 'vue';
import Router from 'vue-router';
// import auth from '@/auth';

import Auth from '@/components/Auth';
import ListBetsByUser from '@/components/ListBetsByUser';
import ListBets from '@/components/ListBets';
import ListAllBets from '@/components/ListAllBets';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    // {
    //   path: '*',
    //   redirect: '/auth',
    // },
    {
      path: '/auth',
      name: 'auth',
      component: Auth,
      meta: {
        guestOnly: true,
      },
    },
    {
      path: '/users/:user', // show user
      name: 'user',
      component: ListBetsByUser,
      meta: {
        requireAuth: false,
      },
    },
    {
      path: '/:user/:wager/:terms', // show bet
      name: 'singlebet',
      component: ListBets,
      meta: {
        requireAuth: true,
      },
    },
    {
      path: '/', // show all
      name: 'allbets',
      component: ListAllBets,
      meta: {
        requireAuth: true,
      },
    },
  ],
});

// router.beforeEach((to, from, next) => {
//   const currentUser = auth.user();
//   // const requireAuth = to.matched.some(record => record.meta.requireAuth);
//   // const guestOnly = to.matched.some(record => record.meta.guestOnly);
//   if (!currentUser) {
//     next('auth');
//   } else {
//     next();
//   }
// });

export default router;
