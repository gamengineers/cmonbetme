/* https://medium.com/@michaljurkowski/how-to-make-basic-authentication-in-vue-js-using-google-firebase-e3ec7dad274 */
import firebase from 'firebase';
import firebaseui from 'firebaseui';

const config = {
  apiKey: 'AIzaSyDhXOS02sYGxXRE7HdjoiHnY4vEm-ncdYc',
  authDomain: 'cmonbetme.firebaseapp.com',
  databaseURL: 'https://cmonbetme.firebaseio.com',
  projectId: 'cmonbetme',
  storageBucket: 'cmonbetme.appspot.com',
  messagingSenderId: '582934344892',
};

const auth = {

  context: null, // main vue instance
  uiConfig: null,
  ui: null,

  init(context) {
    this.context = context;

    firebase.initializeApp(config);

    this.uiConfig = {
      signInSuccessUrl: 'allbets',
      signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      ],
    };

    this.ui = new firebaseui.auth.AuthUI(firebase.auth());

    firebase.auth().onAuthStateChanged((user) => {
      this.context.$store.dispatch('user/setCurrentUser');
      // const requireAuth = this.context.$route.matched.some(record => record.meta.requireAuth);
      // const guestOnly = this.context.$route.matched.some(record => record.meta.guestOnly);
      if (!user) {
        this.context.$router.push('auth');
      } else {
        this.context.$router.push('/');
      }
    });
  },

  authForm(container) {
    this.ui.start(container, this.uiConfig);
  },

  user() {
    return this.context ? firebase.auth().currentUser : null;
  },

  logout() {
    firebase.auth().signOut();
  },

};

export default auth;
