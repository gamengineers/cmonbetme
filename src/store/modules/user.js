// import Vue from 'vue'; // necessary?
import auth from '@/auth';

const getters = {
  user: state => state.user,
  isLogged: state => (state.user !== null),
};

const mutations = {
  setUser: (state, user) => {
    const myState = state;
    myState.user = user;
  },
};

const actions = {
  setCurrentUser: ({ commit }) => {
    commit('setUser', auth.user());
  },
};

const state = {
  user: null,
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
